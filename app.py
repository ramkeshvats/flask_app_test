#!/usr/bin/env python3
# coding=utf-8

from flask import Flask, render_template, request

import logging

app = Flask(__name__)


@app.route('/<file_name>')
@app.route('/', defaults={'file_name': 'file1'})
def read_file_data(file_name):
    args = request.args.copy()
    start_line = args.get('start_line', '0')
    error_str = None
    content = []
    try:
        with open('static_files/{}.txt'.format(file_name), 'r', encoding='utf8', errors='ignore') as fp:
            content = fp.readlines()
    except FileNotFoundError as e:
        error_str = "File with name '{}' not exists".format(file_name)
        logging.error(error_str)
    except Exception as e:
        error_str = "Something bad happen while reading file: {}".format(file_name)
        logging.error("Exception: {} occurs in file read operation".format(e))
    end_line = args.get('end_line', str(len(content)))
    try:
        content = content[int(start_line):int(end_line)]
    except ValueError as v:
        error_str = "start_line and end_line params should be numeric"
        logging.error(error_str)
        return render_template('index.html', file_content=[], error_str=error_str)
    if int(start_line) >= int(end_line) or int(start_line) >= len(content):
        error_str = "`start_line` param should be less than `end_line` params and length of file"
        logging.error(error_str)
        return render_template('index.html', file_content=[], error_str=error_str)

    return render_template('index.html', file_content=content, error_str=error_str)
